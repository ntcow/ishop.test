import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus';
import router from "./router/index.js";
import './style/main.less'
import 'element-plus/lib/theme-chalk/index.css';
import VueI18n from './language'
router.beforeEach((to, from, next) => {
    if (to.meta.title) {
      document.title = to.meta.title;
    }
    next();
  });
  // 通过选项创建 VueI18n 实例
const app = createApp(App)
app.use(ElementPlus)
app.use(router);
app.use(VueI18n);
app.mount("#app");




