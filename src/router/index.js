import { createWebHistory, createRouter } from "vue-router";

const history = createWebHistory();
const router = createRouter({
  history, // 路由模式
  routes: [
    {
      path: "/login",
      name: "login",
      meta: { title: "登录" },
      component: () => import("../components/Login.vue"),
    },
    {
      path: "",
      // redirect: '/home', // 初始化跳转页
      name: "index",
      component: () => import("../components/Index.vue"),
      children: [
        {
          path: "/home", // 用户管理页面
          name: "home",
          component: () => import("../views/Home.vue"),
        },
        {
          path: "/userManagement", // 用户管理页面
          name: "userManagement",
          component: () => import("../views/userManagement/List.vue"),
        },
        {
          path: "/contentManagement", // 用户管理页面
          name: "contentManagement",
          component: () => import("../views/contentManagement/List.vue"),
        },
        {
          path: "/rewardsManagement", // 用户管理页面
          name: "rewardsManagement",
          component: () => import("../views/rewardsManagement/RewardsManagement.vue"),
        },
        {
          path: "/rewardsManagement/detail", // 用户管理页面
          name: "rewardsManagementDetail",
          component: () => import("../views/rewardsManagement/RewardsManagementDetail.vue"),
        },
      ],
    },
    {
      path: "/404",
      name: "404",
      meta: { title: "404" },
      component: () => import("../components/404.vue"),
    },
  ],
});

export default router;
