import { createI18n } from 'vue-i18n'
import message from './index.js'
const i18n = createI18n({
    locale: localStorage.lang || 'ch',
    message    
})
export default i18n