module.exports = {
    create_btn: 'CREATE',
    search_btn: 'SEARCH',
    sort_by: 'Sort by',
    status: 'Status',
    posted_by:'Posted by',
    disable: 'Disable',
}